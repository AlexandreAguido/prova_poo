
public class TesteIntegerSet {
	public static void testeConstrutor() {
		int[] itens = {1,2,3,100};
		IntegerSet A = new IntegerSet();
		IntegerSet B = new IntegerSet(itens);
		System.out.println(A.toSetString()); // = ""
		System.out.println(B.toSetString()); // = "1 2 3 100"
	
	}
	
	public static void testeUnion() {
		int[] itens = {1,2,3}, itens2 = {98,99,100};
		IntegerSet A = new IntegerSet(itens2);
		IntegerSet B = new IntegerSet(itens);
		IntegerSet C = A.union(B);
		System.out.println(C.toSetString()); // == "1 2 3 98 99 100" 
	}
	
	public static void testeIntersection() {
		int[] itens = {1,2,3};
		IntegerSet A = new IntegerSet();
		IntegerSet B = new IntegerSet(itens);
		A.insertElement(0);
		A.insertElement(1);
		A.insertElement(2);
		IntegerSet C = A.intersection(B);
		System.out.println(C.toSetString()); // == "1 2" 
	}
	
	public static void main(String[] args) {
		testeConstrutor();
		testeUnion();
		testeIntersection();
	}
}
