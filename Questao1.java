import java.util.Scanner;

public class Questao1 {

	public static void main(String[] args) {
		double divida, taxaJuros, parcelas, parcelaAtual, totalPago = 0 ;
		Scanner inp = new Scanner(System.in);
		System.out.print("Digite o valor da divida: ");
		divida = inp.nextDouble();
		System.out.print("Digite taxa de juros: ");
		taxaJuros = inp.nextDouble();
		if(taxaJuros >= 1) taxaJuros /= 100;
		System.out.print("Digite o numero de parcelas: ");
		parcelas = inp.nextDouble();
		
		while(parcelas > 0) {
			divida *= 1 + taxaJuros;
			parcelaAtual = divida / parcelas;
			totalPago += parcelaAtual;
			//System.out.printf("Parcela %.0f,  = %.2f\n", parcelas, parcelaAtual);
			divida -= parcelaAtual;
			parcelas -= 1;
		}
		System.out.printf("Total pago: %.2f", totalPago);
		inp.close();
	}

}
