import java.util.Scanner;

public class Questao2 {

	public static String calculaQuadrante(int x, int y) {
		String msg = "O Ponto (%d, %d) est�";
		if(x == 0 && y == 0) return msg + " na origem";
		if(x == 0) return msg + " no eixo Y";
		if(y == 0) return msg + " no eixo X";
		if(x > 0 && y > 0) return msg + " no Q1";
		if(x < 0 && y > 0) return msg + " no Q2";
		if(x < 0 && y < 0) return msg + " no Q3";
		return msg + " no Q4";
	}
	
	public static void main(String[] args) {
		int x, y, n;
		Scanner inp = new Scanner(System.in);
		System.out.println("Digite a quantidade de pares: ");
		n = inp.nextInt();
		for( int i = 0; i < n; i++) {
			System.out.println("Valor de X:");
			x = inp.nextInt();
			System.out.println("Valor de Y:");
			y = inp.nextInt();
			System.out.printf(calculaQuadrante(x, y) + "\n\n", x, y);
		}

	}

}
