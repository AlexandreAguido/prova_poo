
public class IntegerSet {
	boolean[] itens;
	
	public IntegerSet() {
		this.itens = new boolean[101];
	}
	
	public IntegerSet(int[] itens) {
		this.itens = new boolean[101];
		for(int i: itens) {
			this.itens[i] = true;
		}
	}
	
	public void insertElement(int k) {
		itens[k] = true;
	}
	
	public void deleteElement(int m) {
		itens[m] = false;
	}
	
	public IntegerSet union(IntegerSet A) {
		IntegerSet B = new IntegerSet();
		for(int i = 0; i <= 100; i++) {
			if(this.itens[i] || A.itens[i]) {
				B.insertElement(i);
			}
		}
		return B;
	}
	
	public IntegerSet intersection(IntegerSet A) {
		IntegerSet B = new IntegerSet();
		for(int i = 0; i <= 100; i++) {
			if(this.itens[i] && A.itens[i]) {
				B.insertElement(i);
			}
		}
		return B;
	}
	
	public String toSetString() {
		String strItens = "";
		for(int i = 0; i <= 100; i++) {
			if(this.itens[i]) {
				strItens += Integer.toString(i) + " ";
			}
		}
		return strItens;
	}
	
}
