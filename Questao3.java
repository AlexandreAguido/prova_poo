import java.util.Scanner;

public class Questao3 {
	
	static long[] fib_vetor;
	
	public static long fibonacci(int n) {
		if( n < 2) {
			return n;
		}
		if(fib_vetor[n] != 0) {
			return fib_vetor[n];
		}
		fib_vetor[n] = fibonacci(n - 1) + fibonacci(n - 2);
		return fib_vetor[n];
	}
	
	public static void main(String[] args) {
		int n;
		Scanner inp = new Scanner(System.in);
		System.out.println("Digite o valor de n: ");
		n = inp.nextInt();
		fib_vetor = new long[n+1];
		System.out.printf("Fibonacci de %d = %d", n, fibonacci(n));
	}

}
